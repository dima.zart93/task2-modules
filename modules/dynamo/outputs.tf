output "name" {
  description = "Name (id) of the DynamoDB"
  value       = aws_dynamodb_table.dynamodb.id
}
