variable "user_data" {
  description = "File with userdata"
  type = string  
}

variable "key_name" {
    description = "Key pair"
    type = string
    default = "Test"
}

variable "network_interface_id" {
  description = "NIC for EC2-instance"
  type = string
}

variable "tags" {
  description = "Tags to set on the EC2-instance."
  type        = map(string)
  default     = {}
}

variable "OS" {
  type = string  
}

variable "instance_type" {
  type = string
}