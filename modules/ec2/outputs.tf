output "name" {
  description = "Name (id) of the EC2-instance"
  value       = aws_instance.ec2.id
}

output "ip" {
  description = "Public IP of the EC2-instance"
  value       = aws_instance.ec2.public_ip
}

output "key" {
  description = "Key for EC2-instance"
  value       = aws_instance.ec2.key_name
}