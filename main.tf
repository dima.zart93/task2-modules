module "s3" {
    source = "./modules/s3"
    bucket_name = "${var.name}-${var.namespace}-bucket"
    tags = {
        Name   = var.name
        Namespace = var.namespace
    }
}  

module "dynamo" {
    source = "./modules/dynamo"
    lock_name         = "${var.name}-locks"
    tags = {
        Name   = var.name
        Namespace = var.namespace
    }
}
module "vpc" {
    source = "./modules/vpc"
    cidr_block = var.cidr_block
    network = var.network
    ip_address = var.ip_address
    tags = {
        Name   = var.name
        Namespace = var.namespace
    }
}


module "ec2" {
    source = "./modules/ec2"
    OS = var.OS
    instance_type = var.instance_type
    network_interface_id = module.vpc.vpc_nic
    user_data = "${file("userdata.sh")}"
    tags = {
        Name   = var.name
        Namespace = var.namespace
    }
}